# Personal Web Desktop (PWD)

## Introduction

This is a single page application (SPA) with chat integration against a web socket server. The backend (server-side code) of this project will be given, and this part of the project is the client-side code.

## 1. The Memory sub-app

This sub-application is a simple [memory game](https://en.wikipedia.org/wiki/Concentration_(card_game)).

In a memory game, several pairs of tiles are placed face down in a grid. The point of the game is to flip over tiles and match the pairs together. If the images on the facing tiles match, the matching tiles are removed. If the images do not match, the tiles are flipped back face down. The object of the game is to find all pairs. The game is over when all the tiles are gone.

The Memory sub-app should be extended with at least one additional custom feature.

## 2. The Messages sub-app

This sub-application is a course-wide message chat using Web Sockets. Think of this application as a regular message client like WhatsApp, Signal, FB Messenger, or iMessage. 

The Messages sub-app should be extended with at least one additional custom feature.

## 3. The Painting sub-app

This sub-application is a painting app that allows the user to draw using different colors and brush sizes. 

