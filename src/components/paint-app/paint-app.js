/**
 * The script file of the painting application.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

/**
 * Define template.
 */
const template = document.createElement('template')
template.innerHTML = `
<style>
#main-div {
  height: 420px;
}

#blank {
  background-color: white;
  margin: 10px;
}

#color-container {
  float: right;
  width: 20%;
  height: 100%;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  text-align: center;
  align-content: center;
}

.btn {
  width: 40px;
  height: 30px;
  margin: 4px;
  border-radius: 3px;
  font-size: 10px;
  padding: 0px;
}

.style-btn {
  width: 40px;
  height: 30px;
  margin: 4px;
  border-radius: 3px;
  font-size: 10px;
  padding: 0px;
}

.radio {
  width: 35px;
  margin: 5px;
}

.colors {
  width: 30px;
  height: 30px;
  margin: 5px;
  border-radius: 5px;
}

.colors:hover {
  cursor: pointer;
}

#pink {
  background-color: #ff9ced;
}

#red {
  background-color: #ff4040;
}

#orange {
  background-color: #ff9340;
}

#yellow {
  background-color: #ffe940;
}

#lightgreen {
  background-color: #93ff40;
}

#green {
  background-color: #2b9416;
}

#turquoise {
  background-color: #52ffd7;
}

#lightblue {
  background-color: #52fcff;
}

#blue {
  background-color: #52b1ff;
}

#purple {
  background-color: #9d52ff;
}

#gray {
  background-color: #ababab;
}

#black {
  background-color: #000000;
}

input {
  border: solid black 2px;
}
</style>
<div id="main-div">
  <canvas id="blank" width="320" height="400"></canvas>
  <div id="color-container">
    <div id="pink" class="colors"></div>
    <div id="red" class="colors"></div>
    <div id="orange" class="colors"></div>
    <div id="yellow" class="colors"></div>
    <div id="lightgreen" class="colors"></div>
    <div id="green" class="colors"></div>
    <div id="turquoise" class="colors"></div>
    <div id="lightblue" class="colors"></div>
    <div id="blue" class="colors"></div>
    <div id="purple" class="colors"></div>
    <div id="gray" class="colors"></div>
    <div id="black" class="colors"></div>
    <button class="style-btn" id="round">Round</button>
    <button class="style-btn" id="square">Square</button>
    <p>Size</p>
    <input type="radio" name="size" value="1" class="radio">
    <input type="radio" name="size" value="3" class="radio">
    <input type="radio" name="size" value="5" class="radio">
    <input type="radio" name="size" value="8" class="radio">
    <input type="radio" name="size" value="12" class="radio">
    </br>
    </br>
    <button class="btn" id="clear-btn">Clear</button>
  </div>
</div>
`

/**
 * Defines custom element.
 */
customElements.define('paint-app',
  /**
   * The class for the painting app.
   */
  class extends HTMLElement {
    /**
     * Creates an instance.
     */
    constructor () {
      super()
      this.attachShadow({ mode: 'open' })
        .appendChild(template.content.cloneNode(true))
      this.mainDiv = this.shadowRoot.getElementById('main-div')
      this.blank = this.shadowRoot.getElementById('blank')
      this.ctx = this.blank.getContext('2d')
      this.ctx.strokeStyle = 'black'
      this.ctx.lineWidth = 5
      this.ctx.lineCap = 'round'
      this.x = 0
      this.y = 0
      this.mouseDown = false
    }

    /**
     * Sets the current position.
     *
     * @param {object} event - An event.
     */
    setPosition (event) {
      const currX = this.blank.getBoundingClientRect().x
      const currY = this.blank.getBoundingClientRect().y
      this.x = event.clientX - currX
      this.y = event.clientY - currY
    }

    /**
     * Draws a line following the cursor.
     *
     * @param {object} event - An event.
     */
    move (event) {
      const currX = this.blank.getBoundingClientRect().x
      const currY = this.blank.getBoundingClientRect().y
      const nowX = event.clientX - currX
      const nowY = event.clientY - currY

      this.ctx.beginPath()
      this.ctx.moveTo(this.x, this.y)
      this.ctx.lineTo(nowX, nowY)
      this.ctx.stroke()
      this.ctx.closePath()

      this.x = nowX
      this.y = nowY
    }

    /**
     * Sets the event that changes the color.
     */
    colorEvent () {
      const colors = this.shadowRoot.querySelectorAll('.colors')
      for (const color of colors) {
        color.addEventListener('click', () => {
          this.ctx.strokeStyle = (window.getComputedStyle(color).backgroundColor).toString()
        })
      }
    }

    /**
     * Sets the event that sets the size of the brush.
     */
    brushSizeEvent () {
      const sizeOptions = this.shadowRoot.querySelectorAll('.radio')
      for (const option of sizeOptions) {
        option.addEventListener('click', () => {
          this.ctx.lineWidth = option.value
        })
      }
    }

    /**
     * Sets the event that clears the canvas.
     */
    clearEvent () {
      const clearBtn = this.shadowRoot.getElementById('clear-btn')
      clearBtn.addEventListener('click', () => {
        this.ctx.clearRect(0, 0, this.blank.width, this.blank.height)
      })
    }

    /**
     * Sets the event that changes the brush style.
     */
    brushStyleEvent () {
      const styleOptions = this.shadowRoot.querySelectorAll('.style-btn')
      for (const option of styleOptions) {
        option.addEventListener('click', () => {
          // Sets the width of the line to the id of the element.
          this.ctx.lineCap = option.id
        })
      }
    }

    /**
     * Calls event methods and sets eventlisteners on application.
     */
    connectedCallback () {
      this.colorEvent()
      this.brushSizeEvent()
      this.clearEvent()
      this.brushStyleEvent()

      this.blank.addEventListener('mousedown', event => {
        this.setPosition(event)
        this.mouseDown = true

        this.blank.addEventListener('mouseup', event => {
          this.mouseDown = false
        })
      })

      this.blank.addEventListener('mousemove', event => {
        if (this.mouseDown) {
          this.move(event)
        }
      })
    }
  })
