/**
 * The script file of the message application.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import './come-alive.js'

/**
 * Define template.
 */
const template = document.createElement('template')
template.innerHTML = `
<style>
#text-box {
  display: flex;
  flex-wrap: wrap;
  width: 93%;
  padding: 20px;
  border-bottom-left-radius: 20px;
  border-bottom-right-radius: 20px;
  background-color: #63CDDF;
}

textarea {
  resize: none;
  width: 55%;
  outline: none;
  border: none;
}

p {
  font-family: 'Karla', sans-serif;
}

input {
  cursor: pointer;
  width: 60px;
  height: 40px;
  border-radius: 10px;
  border: 2px solid black;
  box-shadow: 4px 4px;
  font-family: 'Comfortaa', cursive;
  background-color: #Ffd76e;
}

input:active {
  position: relative;
  box-shadow: none;
  top: 4px;
  left: 4px;
}

#send-btn {
  float: right;
  margin-left: 40px;
  margin-top: 18px;
}

#save-btn {
  margin: auto;
}

#change-btn {
  cursor: pointer;
  font-family: 'Comfortaa', cursive;
  border: none;
  outline: none;
  background-color: transparent;
  text-decoration: underline;
  text-decoration-color: #fb7b7b;
  text-decoration-thickness: 3px;
}

#change-btn:hover {
  text-decoration: none;
  color: #fb7b7b;
}

#name-container {
  float: left;
  font-family: 'Karla', sans-serif;
  padding: 6px;
  margin-right: 20px;
  text-align: center;
}

#input-name {
  text-align: center;
  margin-left: -6px;
  margin-right: 20px;
}

#my-msg {
  position: relative;
  font-family: 'Karla', sans-serif;
  background-color: #fbdb8b;
  font-size: 14px;
  padding: 15px;
  width: max-content;
  float: right;
  clear: both;
  border: 2px solid black;
  border-radius: 40px;
  margin-right: 20px;
  box-shadow: 5px 5px black;
}

#msg {
  position: relative;
  font-size: 14px;
  clear: both;
  font-family: 'Karla', sans-serif;
  width: max-content;
  background-color: #fb7b7b;
  border: 2px solid black;
  border-radius: 40px;
  padding: 15px;
  margin-left: 20px;
  box-shadow: -4px 4px 0 0 #000000;
}

.user {
  font-family: 'Poppins', sans-serif;
  font-size: 14px;
  clear: both;
  margin-bottom: -7px;
}

#me {
  float: right;
  margin-right: 20px;
  margin-top: -4px;
}

#other-user {
  margin-left: 20px;
}

#msg-box {
  display: flex;
  flex-direction: column-reverse;
  height: 350px;
  overflow-y: scroll;
}

#msg-container {
  position: relative;
  z-index: 1;
}

#smile {
  z-index: 0;
  position: absolute;
  margin-bottom: 300px;
}
</style>
<div id="main-div">
  <div id="msg-box">
    <div id="msg-container">
    </div>
  </div>
  <div id="text-box">
    <textarea rows="4" id="text-area" placeholder='Write something...\nOr try typing: "Wake up!"'></textarea>
    <input id="send-btn" type="submit" value="Send"></input>
  </div>
</div>
`

/**
 * Defines custom element.
 */
customElements.define('message-app',
  /**
   * The class for the message app.
   */
  class extends HTMLElement {
    /**
     * Creates an instance.
     */
    constructor () {
      super()
      this.attachShadow({ mode: 'open' }).appendChild(template.content.cloneNode(true))
      this.socket = new WebSocket('wss://cscloud6-127.lnu.se/socket/')
      this.mainDiv = this.shadowRoot.getElementById('main-div')
      this.boxDiv = this.shadowRoot.getElementById('text-box')
      this.msgDiv = this.shadowRoot.getElementById('msg-box')
      this.username = ''
      this.currentMessage = ''
    }

    /**
     * Connects to websocket and begins listening to it.
     */
    listenSocket () {
      this.socket.addEventListener('message', event => {
        const message = document.createElement('p')
        const dataObj = JSON.parse(event.data)
        // Ignores heartbeat messages and messages sent by the current user.
        if (dataObj.type === 'heartbeat' || dataObj.data === this.currentMessage) {
          return
        }

        message.textContent = dataObj.data
        message.id = 'msg'

        const user = document.createElement('p')
        user.textContent = dataObj.username
        user.id = 'other-user'
        user.classList.add('user')

        const msgContainer = this.shadowRoot.getElementById('msg-container')
        msgContainer.appendChild(user)
        msgContainer.appendChild(message)
        this.msgDiv.appendChild(msgContainer)
      })
    }

    /**
     * Sets the nickname for the user.
     */
    setName () {
      this.username = document.cookie.split('=')[1]
      const inputName = document.createElement('p')
      inputName.id = 'input-name'
      inputName.textContent = 'Enter a nickname:'

      const inputMsg = this.shadowRoot.getElementById('text-area')

      this.boxDiv.insertBefore(inputName, inputMsg)
      const saveBtn = this.shadowRoot.getElementById('send-btn')
      saveBtn.id = 'save-btn'
      saveBtn.value = 'Save'
      /**
       * Sets a cookie that stores the user's nickname.
       */
      const setCookie = () => {
        const inputMsg = this.shadowRoot.getElementById('text-area')
        document.cookie = 'nickname=' + inputMsg.value
        this.boxDiv.removeChild(this.shadowRoot.getElementById('input-name'))

        this.displayName()

        this.username = inputMsg.value

        saveBtn.id = 'send-btn'
        saveBtn.value = 'Send'

        this.sendEvent()
        saveBtn.removeEventListener('click', setCookie)
      }
      saveBtn.addEventListener('click', setCookie)
    }

    /**
     * Handles the event for sending a message.
     */
    sendEvent () {
      const sendBtn = this.shadowRoot.getElementById('send-btn')
      sendBtn.addEventListener('click', () => {
        const data = this.shadowRoot.getElementById('text-area').value
        // Additional feature, listens for specific messages.
        if (data.toLowerCase() === 'wake up!') {
          const smile = document.createElement('come-alive')
          smile.id = 'smile'
          this.msgDiv.appendChild(smile)
          smile.comeAlive()
        }
        if (data.toLowerCase() === 'goodnight.') {
          const smile = this.shadowRoot.getElementById('smile')
          if (smile) {
            this.msgDiv.removeChild(smile)
          }
        }

        const fullMessage = {
          type: 'message',
          data: data,
          username: document.cookie.split('=')[1],
          key: 'eDBE76deU7L0H9mEBgxUKVR0VCnq0XBd'
        }

        this.currentMessage = data
        this.socket.send(JSON.stringify(fullMessage))

        const user = document.createElement('p')
        user.textContent = this.username
        user.id = 'me'
        user.classList.add('user')

        const message = document.createElement('p')
        message.textContent = data
        message.id = 'my-msg'

        const msgContainer = this.shadowRoot.getElementById('msg-container')
        msgContainer.appendChild(user)
        msgContainer.appendChild(message)
      })
    }

    /**
     * Displays the user's nickname.
     */
    displayName () {
      const nameDiv = document.createElement('div')
      nameDiv.id = 'name-container'

      const name = document.cookie.split('=')[1]
      const nameElem = document.createElement('p')
      nameElem.textContent = name
      nameElem.id = 'display-name'

      nameDiv.appendChild(nameElem)

      const changeBtn = document.createElement('button')
      changeBtn.textContent = 'Change'
      changeBtn.id = 'change-btn'

      nameDiv.appendChild(changeBtn)
      this.boxDiv.insertBefore(nameDiv, this.shadowRoot.getElementById('text-area'))

      this.changeName()
    }

    /**
     * Adds event to change name button.
     */
    changeName () {
      const changeBtn = this.shadowRoot.getElementById('change-btn')
      changeBtn.addEventListener('click', () => {
        this.boxDiv.removeChild(this.shadowRoot.getElementById('name-container'))
        this.setName()
      })
    }

    /**
     * The start of the application.
     */
    connectedCallback () {
      this.listenSocket()
      // If there isn't a cookie set, ask the user to enter a nickname.
      if (document.cookie) {
        this.username = document.cookie.split('=')[1]
        this.displayName()
        this.sendEvent()
      } else {
        this.setName()
      }
    }
  }
)
