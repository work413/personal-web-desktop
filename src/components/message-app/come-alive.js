/**
 * The script file for the additional feature of the chat application.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

/**
 * Define template.
 */
const template = document.createElement('template')
template.innerHTML = `
 <style>
.come-alive {
    position: absolute;
}

.come-alive.hidden {
    display: none;
}

.eye.show {
  background-color: black;
  width: 60px;
  height: 60px;
  margin-top: 80px;
  border-radius: 50%;
  animation: open 1s;
}

.pupil.show {
  width: 30px;
  height: 30px;
  border-radius: 50%;
  background-color: white;
  margin-top: 10px;
  margin-left: 25px;
  animation: pupils 1s;
}

.small-pup.show {
  background-color: white;
  width: 10px;
  height: 10px;
  border-radius: 50%;
  margin-left: 10px;
  animation: smaller 1s;
}

#eye-one {
  margin-left: 100px;
  position: absolute;
}

#eye-two {
  margin-left: 400px;
  position: absolute;
}

.cheeks.show {
  width: 70px;
  height: 35px;
  background-color: #F1ACD7;
  border-radius: 50%;
  margin-top: 150px;
  position: absolute;
  animation: blush 1.3s 1.5s backwards;
}

#cheek-one {
  margin-left: 65px;
}

#cheek-two {
  margin-left: 425px;
}

#smile {
  background-color: #E38080;
  width: 250px;
  height: 70px;
  margin-left: 160px;
  border-radius: 50%;
  margin-top: 150px;
}
#smile-bow.show {
  background-color: #95EBF9;
  width: 300px;
  height: 80px;
  margin-top: -100px;
  margin-left: 140px;
  border-radius: 50%;
  bottom: -30px;
  animation: smile 1.3s;
}

@keyframes open {
    0%   {
      height: 0px;
      width: 40px;
      margin-top: 100px;
    }
    100%  {
      height: 60px;
      width: 60px;
      margin-top: 80px;
    }
  }

  @keyframes pupils {
    0%   {
      width: 20px;
      height: 0px;
      margin-top: 0px;
    }
    100%  {
      width: 30px;
      height: 30px;
      margin-top: 10px;
    }
  }

  @keyframes smaller {
    0%   {
      height: 0px;
      margin-top: 0px;
    }
    100%  {
      height: 10px;
    }
  }

  @keyframes smile {
    0%  {
      margin-top: -75px;
    }
    100%  {
      margin-top: -100px;
    }
  }

  @keyframes blush {
    0%  {
      opacity: 0%;
    }
    100%  {
      opacity: 100%;
    }
  }
 </style>
 <div class="come-alive hidden">
  <div id="eye-one" class="eye">
    <div id="pupil-one" class="pupil"></div>
    <div id="smaller-one" class="small-pup"></div>
  </div>
  <div id="eye-two" class="eye">
    <div id="pupil-two" class="pupil"></div>
    <div id="smaller-two" class="small-pup"></div>
  </div>
  <div id="cheek-one" class="cheeks"></div>
  <div id="cheek-two" class="cheeks"></div>
  <div id="smile"></div>
  <div id="smile-bow"></div>
</div>
 `

/**
 * Defines custom element.
 */
customElements.define('come-alive',
  /**
   * The class for the come alive functionality.
   */
  class extends HTMLElement {
    /**
     * Creates an instance.
     */
    constructor () {
      super()
      this.attachShadow({ mode: 'open' }).appendChild(template.content.cloneNode(true))
    }

    /**
     * Displays a smiling face.
     */
    comeAlive () {
      const alive = this.shadowRoot.querySelector('.come-alive')
      alive.classList.remove('hidden')
      this.elementList('.eye')
      this.elementList('.pupil')
      this.elementList('.small-pup')
      this.elementList('.cheeks')

      const bow = this.shadowRoot.querySelector('#smile-bow')
      this.showElem(bow)
    }

    /**
     * Creates a NodeList of all elements of the same class.
     *
     * @param {string} cls - A class name.
     */
    elementList (cls) {
      const nodes = this.shadowRoot.querySelectorAll(cls)
      nodes.forEach(elem => {
        this.showElem(elem)
      })
    }

    /**
     * Adds show to an element's class list.
     *
     * @param {object} elem - An element.
     */
    showElem (elem) {
      elem.classList.add('show')
    }
  }
)
