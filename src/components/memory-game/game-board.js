/**
 * The script file for game board element.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import './make-card.js'

/**
 * Define template.
 */
const template = document.createElement('template')
template.innerHTML = `
<style>
#main-div {
  display: flex;
  flex-wrap: wrap;
  background-color: #DFF5FF;
  padding: 10px;
}

.cards {
  position: relative;
  width: 22%;
  margin: auto;
  transform-style: preserve-3d;
  transition: transform 0.3s;
}

.cards.flip {
  transform: rotateY(180deg);
}

.cards.paired {
  transform: rotateY(180deg);
}

#highlight {
  border: 3px solid #24BCE1;
  border-radius: 10px;
  text-align: center;
  padding-top: 1px;
  width: 20%;
}
</style>
<div id="main-div">
</div>
`
let flippedCard = false
let locked = false
let firstCard
let secondCard

/**
 * Defines custom element.
 */
customElements.define('game-board',
  /**
   * The class for the memory game.
   */
  class extends HTMLElement {
    /**
     * Creates an instance.
     */
    constructor () {
      super()
      this.attachShadow({ mode: 'open' }).appendChild(template.content.cloneNode(true))
      this.board = this.shadowRoot.getElementById('main-div')
      this.cards = ''
      this.size = ''
    }

    /**
     * Creates a card.
     *
     * @param {string} name - The name of the card.
     * @param {string} url - Url to an image.
     * @returns {object} - The card element.
     */
    createCard (name, url) {
      const card = document.createElement('make-card')
      card.setImg(url)
      card.setName(name)
      card.createCard()
      card.classList.add('cards')
      return card
    }

    /**
     * Adds the click-event to each card.
     */
    cardEvent () {
      this.cards.forEach(card => {
        card.addEventListener('click', this.flipCards)
        card.addEventListener('click', () => {
          // Dispatches custom event when all cards are paired.
          if (this.shadowRoot.querySelectorAll('.paired').length === this.cards.length) {
            const gameFinished = new CustomEvent('game-finished')
            this.dispatchEvent(gameFinished)
          }
        })
      })
    }

    /**
     * Flips selected card, and handles the event.
     */
    flipCards () {
      if (locked) return
      if (this === firstCard) return
      if (this.classList.contains('paired')) return
      this.classList.add('flip')
      // If this is the first card.
      if (!flippedCard) {
        flippedCard = true
        firstCard = this
        // If this is the second chosen card.
      } else {
        flippedCard = false
        secondCard = this

        // Let the cards stay flipped if they match each other.
        if (firstCard.dataName === secondCard.dataName) {
          firstCard.classList.add('paired')
          secondCard.classList.add('paired')
          firstCard.classList.remove('flip')
          secondCard.classList.remove('flip')
          firstCard.removeEventListener('click', this.flipCards)
          secondCard.removeEventListener('click', this.flipCards)
          // Flips the cards to their back if they don't match.
        } else {
          locked = true
          setTimeout(() => {
            firstCard.classList.remove('flip')
            secondCard.classList.remove('flip')
            locked = false
          }, 800)
        }
      }
    }

    /**
     * Creates a small sized game board.
     */
    smallBoard () {
      this.board.textContent = ''
      this.board.appendChild(this.createCard('noodles', 'images/2202_R0lVIEFOTiAwNDAtODg.png'))
      this.board.appendChild(this.createCard('noodles', 'images/2202_R0lVIEFOTiAwNDAtODg.png'))
      this.board.appendChild(this.createCard('sandwich', 'images/2227_R0lVIEFOTiAwNDYtODI.png'))
      this.board.appendChild(this.createCard('sandwich', 'images/2227_R0lVIEFOTiAwNDYtODI.png'))
      this.size = 'small'
    }

    /**
     * Creates a medium sized game board.
     */
    mediumBoard () {
      this.smallBoard()
      this.board.appendChild(this.createCard('waffle', 'images/2226_R0lVIEFOTiAwNDItMTAy.png'))
      this.board.appendChild(this.createCard('waffle', 'images/2226_R0lVIEFOTiAwNDItMTAy.png'))
      this.board.appendChild(this.createCard('burger', 'images/2226_R0lVIEFOTiAwNDItNzQ.png'))
      this.board.appendChild(this.createCard('burger', 'images/2226_R0lVIEFOTiAwNDItNzQ.png'))
      this.size = 'medium'
    }

    /**
     * Creates a big sized game board.
     */
    bigBoard () {
      this.mediumBoard()
      this.board.appendChild(this.createCard('sushi', 'images/2202_R0lVIEFOTiAwNDAtOTA.png'))
      this.board.appendChild(this.createCard('sushi', 'images/2202_R0lVIEFOTiAwNDAtOTA.png'))
      this.board.appendChild(this.createCard('fries', 'images/2226_R0lVIEFOTiAwNDItMTAw.png'))
      this.board.appendChild(this.createCard('fries', 'images/2226_R0lVIEFOTiAwNDItMTAw.png'))
      this.board.appendChild(this.createCard('egg', 'images/2226_R0lVIEFOTiAwNDItMTE2.png'))
      this.board.appendChild(this.createCard('egg', 'images/2226_R0lVIEFOTiAwNDItMTE2.png'))
      this.board.appendChild(this.createCard('milk', 'images/2226_R0lVIEFOTiAwNDItNjQ.png'))
      this.board.appendChild(this.createCard('milk', 'images/2226_R0lVIEFOTiAwNDItNjQ.png'))
      this.size = 'big'
    }

    /**
     * Collects all cards.
     */
    allCards () {
      this.cards = this.shadowRoot.querySelectorAll('.cards')
    }

    /**
     * Randomizes the board.
     */
    randomize () {
      const orderNums = [] // A list of numbers for the order of the cards.
      for (let i = 0; i < this.cards.length; i++) {
        orderNums.push(i)
      }
      for (let i = (orderNums.length - 1); i > 0; i--) {
        const j = Math.floor(Math.random() * i)
        const temp = orderNums[i]
        orderNums[i] = orderNums[j]
        orderNums[j] = temp
      }
      // Assigns a number for each card.
      for (let x = 0; x < orderNums.length; x++) {
        this.cards[x].style.order = orderNums[x]
      }
    }

    /**
     * Resets the board.
     */
    reset () {
      this.allCards()
      this.randomize()
      this.cardEvent()
    }

    /**
     * Sets the board and events.
     */
    connectedCallback () {
      this.bigBoard()
      this.reset()
    }
  }
)
