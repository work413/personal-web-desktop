/**
 * The script file for the card element.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

/**
 * Define template.
 */
const template = document.createElement('template')
template.innerHTML = `
<style>
img {
  border-radius: 10%;
  border: 3px solid #95EBF9;
}

#card-back {
    position: absolute;
    width: 90%;
    height: 90%;
    backface-visibility: hidden;
}

#card-front {
    width: 90%;
    height: 90%;
}
</style>
<img src="images/cardback.png" id="card-back" alt="Boy with heart-balloon and smartphone.">
<img id="card-front">
`

/**
 * Defines custom element.
 */
customElements.define('make-card',
  /**
   * The class for the memory game.
   */
  class extends HTMLElement {
    /**
     * Creates an instance.
     */
    constructor () {
      super()
      this.attachShadow({ mode: 'open' }).appendChild(template.content.cloneNode(true))
      this.dataName = ''
      this.imgUrl = ''
    }

    /**
     * Sets the name for the card.
     *
     * @param {string} name - The name of this card.
     */
    setName (name) {
      this.dataName = name
    }

    /**
     * Returns the card's name.
     *
     * @returns {string} - The name of this card.
     */
    getName () {
      return this.dataName
    }

    /**
     * Sets the image of the card.
     *
     * @param {string} url - The url to the image of this card.
     */
    setImg (url) {
      this.imgUrl = url
    }

    /**
     * Returns the image's url.
     *
     * @returns {string} - The url to the image of this card.
     */
    getImg () {
      return this.imgUrl
    }

    /**
     * Creates a card element.
     */
    createCard () {
      const cardFront = this.shadowRoot.getElementById('card-front')
      cardFront.setAttribute('src', this.imgUrl)
      this.setAttribute('data-name', this.dataName)
    }
  }
)
