/**
 * The script file of the memory application.
 * I copied alot of the code from when I did the memory exercise. All commits for this application is in the excercise repository.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import './game-board.js'
import './count-timer.js'

/**
 * Define template.
 */
const template = document.createElement('template')
template.innerHTML = `
<style>
a {
  font-family: 'Karla', sans-serif;
  text-decoration: none;
  color: #A74262;
}

p {
  font-family: 'Karla', sans-serif;
  text-decoration: none;
  font-size: 22px;
}

menu {
  display: flex;
}

#main-div {
  text-align: center;
}

#main-div:focus {
  outline: none;
  border: 3px solid #CF8BB4;
}

#play-btn {
  cursor: pointer;
  width: 100px;
  height: 40px;
  border-radius: 10px;
  border: 2px solid black;
  box-shadow: 4px 4px;
  font-family: 'Comfortaa', cursive;
  background-color: #Ffd76e;
  margin: 30px;
}

count-timer {
  position: aboslute;
  margin-left: 100px;
  width: 50px;
}

.radio-label {
  margin-right: 20px;
  font-family: 'Karla', sans-serif;
  font-size: 20px;
}
</style>
 <menu>
  <label class="radio-label">2x2
   <input type="radio" name="board" class="radio" value="2x2">
   <span></span>
  </label>
  <label class="radio-label">4x2
   <input type="radio" name="board" class="radio" value="4x2">
   <span></span>
  </label>
  <label class="radio-label">4x4
   <input type="radio" name="board" class="radio" value="4x4">
   <span></span>
  </label>
 </menu>
 <div id="main-div" tabindex="-1">
 </div>
 <a href="https://www.vecteezy.com/free-vector/cool">Vectors by Vecteezy</a>`

/**
 * Defines custom element.
 */
customElements.define('memory-game',
  /**
   * The class for the memory game.
   */
  class extends HTMLElement {
    /**
     * Creates an instance.
     */
    constructor () {
      super()
      this.attachShadow({ mode: 'open' })
        .appendChild(template.content.cloneNode(true))
      this.board = this.shadowRoot.getElementById('main-div')
      this.attempts = 0
      this.gameBoard = ''
      this.currentCard = ''
      this.position = 0
      this.firstCard = ''
      this.lastCard = ''
    }

    /**
     * Handles keyboard event.
     */
    keyControls () {
      this.board.addEventListener('keydown', event => {
        // Sets the current card to the first card if it's order index is less than 1.
        if (this.currentCard.length <= 0) {
          this.currentCard = this.firstCard
        }
        this.currentCard.removeAttribute('id')
        switch (event.key) {
          case 'ArrowDown':
            this.down()
            break
          case 'ArrowUp':
            this.up()
            break
          case 'ArrowLeft':
            this.left()
            break
          case 'ArrowRight':
            this.right()
            break
          case 'Enter':
            this.currentCard.id = 'highlight'
            this.currentCard.click()
            return true
        }
      })
    }

    /**
     * Moves position one step to the left.
     */
    left () {
      for (let i = 0; i < this.cards.length; i++) {
        // Moves to the last card if the current card is the first one.
        if (this.position === 0) {
          this.position = this.cards.length - 1
          this.setCurrentCard(this.lastCard)
          return
        } else if (this.cards[i].style.order === (this.position - 1).toString()) {
          this.setCurrentCard(this.cards[i])
          this.position = this.position - 1
          return
        }
      }
    }

    /**
     * Moves position one step to the right.
     */
    right () {
      for (let i = 0; i < this.cards.length; i++) {
        // Moves to the first card if the current card is the last one.
        if (this.position === this.cards.length - 1) {
          this.position = 0
          this.setCurrentCard(this.firstCard)
          return
        } else if (this.cards[i].style.order === (this.position + 1).toString()) {
          this.setCurrentCard(this.cards[i])
          this.position = this.position + 1
          return
        }
      }
    }

    /**
     * Moves position one step up.
     */
    up () {
      for (let i = 0; i < this.cards.length; i++) {
        if (this.gameBoard.size === 'small') {
          // Moves to the first card if the current card is the last one.
          if (this.position === this.cards.length - 1) {
            this.setCurrentCard(this.firstCard)
            this.position = 0
            return
          }
          if (this.cards[i].style.order === (this.position + 1).toString()) {
            // Moves to the right if the game board is small.
            this.setCurrentCard(this.cards[i])
            this.position = this.position + 1
            return
          }
        } else {
          // Stays at first card if current card is first card.
          if (this.position === 0) {
            this.setCurrentCard(this.firstCard)
            return
          }
          if (this.cards[i].style.order === (this.position - 4).toString()) {
            this.setCurrentCard(this.cards[i])
            this.position = this.position - 4
            return
          }
        }
      }
    }

    /**
     * Moves position one step down.
     */
    down () {
      for (let i = 0; i < this.cards.length; i++) {
        if (this.gameBoard.size === 'small') {
          // Moves to the last card if the current card is the first one.
          if (this.position === 0) {
            this.setCurrentCard(this.lastCard)
            this.position = this.cards.length - 1
            return
          }
          if (this.cards[i].style.order === (this.position - 1).toString()) {
            // Moves to the left if the game board is small.
            this.setCurrentCard(this.cards[i])
            this.position = this.position - 1
            return
          }
        } else {
          // Moves to the first card if the current card is the last one.
          if (this.position === this.cards.length - 1) {
            this.setCurrentCard(this.lastCard)
            return
          }
          if (this.cards[i].style.order === (this.position + 4).toString()) {
            this.setCurrentCard(this.cards[i])
            this.position = this.position + 4
            return
          }
        }
      }
    }

    /**
     * Sets the last card on the board.
     */
    setLastCard () {
      for (let i = 0; i < this.cards.length; i++) {
        if (parseInt(this.cards[i].style.order) === this.cards.length - 1) {
          this.lastCard = this.cards[i]
        }
      }
    }

    /**
     * Sets the first card on the board.
     */
    setFirstCard () {
      for (let i = 0; i < this.cards.length; i++) {
        if (parseInt(this.cards[i].style.order) === 0) {
          this.firstCard = this.cards[i]
        }
      }
    }

    /**
     * Sets the current card.
     *
     * @param {object} card - A card object.
     */
    setCurrentCard (card) {
      this.currentCard = card
      card.id = 'highlight'
    }

    /**
     * Sets the size of the game-board with radio buttons.
     */
    setBoardSize () {
      const boardOptions = this.shadowRoot.querySelectorAll('.radio')
      for (const size of boardOptions) {
        size.addEventListener('click', event => {
          if (size.value === '2x2') {
            this.gameBoard.smallBoard()
          } else if (size.value === '4x2') {
            this.gameBoard.mediumBoard()
          } else if (size.value === '4x4') {
            this.gameBoard.bigBoard()
          }
          this.gameBoard.reset()
          this.reset()
        })
      }
    }

    /**
     * Resets the game.
     */
    reset () {
      this.attempts = 0
      const oldTimer = this.shadowRoot.querySelector('count-timer')
      // Removes the old timer.
      if (oldTimer) {
        this.shadowRoot.querySelector('menu').removeChild(oldTimer)
      }
      this.cards = this.gameBoard.shadowRoot.querySelectorAll('.cards')
      this.countAttempts()
      this.setLastCard()
      this.setFirstCard()
      // Sets a timer if there isn't a play-again button present.
      if (!this.shadowRoot.getElementById('play-btn')) {
        const timer = document.createElement('count-timer')
        this.shadowRoot.querySelector('menu').appendChild(timer)
      }
    }

    /**
     * Handles a finished game.
     */
    finishedGame () {
      this.board.textContent = ''

      const playAgain = document.createElement('button')
      playAgain.id = 'play-btn'
      playAgain.textContent = 'Play Again'
      playAgain.tabindex = '-2'

      const attempts = document.createElement('p')
      attempts.textContent = 'Your attempts: ' + (this.attempts + 1)

      const timer = this.shadowRoot.querySelector('count-timer')
      timer.stopTimer()

      const time = document.createElement('p')
      time.textContent = 'Your time: ' + timer.currentTime

      this.shadowRoot.querySelector('menu').removeChild(timer)

      playAgain.addEventListener('click', () => {
        this.setBoard()
        this.reset()
      })

      this.board.appendChild(time)
      this.board.appendChild(attempts)
      this.board.appendChild(playAgain)
    }

    /**
     * Counts the attempts for one game.
     */
    countAttempts () {
      this.cards.forEach(card => {
        card.addEventListener('click', event => {
          const flipClass = this.gameBoard.shadowRoot.querySelectorAll('.flip')

          if (card.classList.contains('flip') && flipClass.length === 2) {
            this.attempts++
          } else if (card.classList.contains('paired')) {
            this.attempts++
          }
        })
      })
    }

    /**
     * Sets the board for a new game.
     */
    setBoard () {
      this.board.textContent = ''
      this.gameBoard = document.createElement('game-board')
      this.gameBoard.addEventListener('game-finished', event => {
        this.finishedGame()
      })
      this.gameBoard.id = 'board'

      this.board.appendChild(this.gameBoard)

      const boardOptions = this.shadowRoot.querySelectorAll('.radio')

      for (const size of boardOptions) {
        // Sets the board to the current value of the checked size button.
        if (size.checked) {
          if (size.value === '2x2') {
            this.gameBoard.smallBoard()
          } else if (size.value === '4x2') {
            this.gameBoard.mediumBoard()
          } else {
            this.gameBoard.bigBoard()
          }
        }
      }
      this.gameBoard.reset()
    }

    /**
     * The start of the application.
     */
    connectedCallback () {
      const boardOptions = this.shadowRoot.querySelectorAll('.radio')
      boardOptions[2].checked = true
      this.setBoard()
      this.setBoardSize()
      this.reset()
      this.keyControls()
    }
  })
