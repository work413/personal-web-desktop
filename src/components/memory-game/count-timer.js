/**
 * The module for the timer component.
 * I copied alot of the functionality from the timer in assignment b2.
 *
 * @author Martina Andersson <ma225gc@student.lnu.se>
 * @version 1.0.0
 */

/**
 * Define template.
 */
const template = document.createElement('template')

template.innerHTML = `
 <style>
 #box {
   width: 50px;
   text-align: center;
   height: 30px;
   padding-top: 3px;
   background-color: #FFD3EE;
   border-radius: 20em;
 }
 
 #clock {
   margin: auto;
   font-family: 'Nunito', sans-serif;
   color: #CF8BB4;
   font-size: 20px;
 }
 </style>
 <div id="box">
   <p id="clock"></p>
 </div>`

/**
 * Define custom element.
 */
customElements.define('count-timer',
  /** Class for count-timer element. */
  class extends HTMLElement {
    /**
     * Class constructor.
     */
    constructor () {
      super()
      this.attachShadow({ mode: 'open' })
        .appendChild(template.content.cloneNode(true))
      this.box = this.shadowRoot.getElementById('box')
      this.clock = this.shadowRoot.getElementById('clock')
      this.currentTime = ''
    }

    /**
     * Set the time.
     */
    setTimer () {
      const clock = this.clock
      let seconds = 0
      let minutes = 0
      setInterval(() => {
        if (seconds === 60) {
          minutes++
          seconds = 0
        }
        clock.innerHTML = minutes + ':' + (seconds++)
      }, 1000)
    }

    /**
     * Stops the timer.
     */
    stopTimer () {
      const timeLeft = this.clock.textContent
      this.currentTime = timeLeft
    }

    /**
     * Removes all elements when the custom element is disconnected from a document's DOM.
     */
    disconnectedCallback () {
      if (typeof (this.box) !== 'undefined' && this.box != null) {
        this.box.textContent = ''
        this.clock.textContent = ''
      }
    }

    /**
     * Starts timer whenever the element is appended into a document's DOM.
     */
    connectedCallback () {
      this.setTimer()
    }
  })
