/**
 * The main script file of the application.
 *
 * @author Martina Andersson <ma225gc@student.lnu.se>
 * @version 1.0.0
 */

import './components/memory-game/index.js'
import './components/message-app/index.js'
import './components/paint-app/index.js'

// If there is a service worker, start listening for the load event.
if ('serviceWorker' in navigator) {
  window.addEventListener('load', async () => {
    try {
      // Register the service worker.
      const registration = await navigator.serviceWorker.register('./sw.js')
      console.log('ServiceWorkers: Registration successful: ' + registration)
    } catch (err) {
      console.log('ServiceWorker: Registration failed.')
    }
  })
}

const chatBtn = document.getElementById('chat-btn')
chatBtn.style.order = '0'
const paintBtn = document.getElementById('paint-btn')
paintBtn.style.order = '1'
const memoryBtn = document.getElementById('memory-btn')
memoryBtn.style.order = '2'

memoryBtn.addEventListener('click', () => {
  setElem('memory-game', 'Memory Game')
})

chatBtn.addEventListener('click', () => {
  setElem('message-app', 'Chat Application')
})

paintBtn.addEventListener('click', () => {
  setElem('paint-app', 'Painting Application')
})

/**
 * Adds element to main document.
 *
 * @param {string} id - The id and name of the element.
 * @param {string} title - Title of element.
 */
function setElem (id, title) {
  const modal = document.createElement('div')
  modal.classList.add('modal')
  modal.id = id

  const bar = document.createElement('div')
  bar.classList.add('bar')
  bar.id = id + '-bar'

  const btn = document.createElement('button')
  btn.classList.add('close-btn')
  btn.textContent = 'X'
  // Add the close window to the button.
  closeWindow(btn)

  const titleElem = document.createElement('h2')
  titleElem.textContent = title
  bar.appendChild(btn)
  bar.appendChild(titleElem)

  const memoBar = document.createElement('div')

  const app = document.createElement(id)
  modal.appendChild(bar)
  modal.appendChild(memoBar)
  modal.appendChild(app)
  document.querySelector('main').appendChild(modal)

  // Makes the element draggable.
  drag(bar, modal)
}

/**
 * Makes an element draggable.
 *
 * @param {object} bar - The bar that the event is added to.
 * @param {object} container - The container that is draggable.
 */
function drag (bar, container) {
  let one = 0
  let two = 0
  let three = 0
  let four = 0

  if (bar) {
    bar.addEventListener('mousedown', event => {
      event = event || window.event
      three = event.clientX
      four = event.clientY
      const siblings = container.parentNode.childNodes

      // Pushes the current element to the front.
      for (const elem of siblings) {
        elem.classList.remove('front')
        elem.classList.add('back')
      }
      container.classList.remove('back')
      container.classList.add('front')

      document.addEventListener('mouseup', event => {
        // Removes the move event when lifting the mouse.
        document.removeEventListener('mousemove', move)
      })

      /**
       * Handles the move event.
       *
       * @param {object} event - An event.
       */
      const move = function (event) {
        event = event || window.event
        event.preventDefault()
        one = three - event.clientX
        two = four - event.clientY
        three = event.clientX
        four = event.clientY

        container.style.top = (container.offsetTop - two) + 'px'
        container.style.left = (container.offsetLeft - one) + 'px'
      }

      document.addEventListener('mousemove', move)
    })
  }
}

/**
 * Closes an application window.
 *
 * @param {object} btn - The button that the event is added to.
 */
function closeWindow (btn) {
  btn.addEventListener('click', () => {
    const parent = btn.parentElement
    const grandParent = parent.parentElement
    // Removes the container of the application from the main element.
    document.querySelector('main').removeChild(grandParent)
  })
}

let position = document.getElementById('chat-btn')
const allBtns = document.getElementsByClassName('dock-btn')

/**
 * Handles keyboard events.
 */
function keyboardEvents () {
  document.body.addEventListener('keydown', event => {
    // Returns if there is an app open.
    if (document.querySelector('main').textContent.length > 0) {
      return
    }
    position.classList.remove('highlight')
    switch (event.key) {
      case 'ArrowDown': {
        moveBackwards()
        break
      }
      case 'ArrowUp': {
        moveForwards()
        break
      }
      case 'ArrowLeft': {
        moveBackwards()
        break
      }
      case 'ArrowRight': {
        moveForwards()
        break
      }
      case 'Enter': {
        position.click()
        break
      }
      default:
    }
  })
}

/**
 * Sets the position when moving forwards.
 */
function moveForwards () {
  if (parseInt(position.style.order) === allBtns.length - 1) {
    // Sets the position to the first button if the current position if the last button.
    position = allBtns[0]
    position.classList.add('highlight')
  } else {
    for (let i = 0; i < allBtns.length; i++) {
      if (allBtns[i].style.order === (parseInt(position.style.order) + 1).toString()) {
        position = allBtns[i]
        position.classList.add('highlight')
        return
      }
    }
  }
}

/**
 * Sets the position when moving backwards.
 */
function moveBackwards () {
  if (parseInt(position.style.order) === 0) {
    // Sets the position to the last button if the current position if the first button.
    position = allBtns[allBtns.length - 1]
    position.classList.add('highlight')
  } else {
    for (let i = 0; i < allBtns.length; i++) {
      if (allBtns[i].style.order === (parseInt(position.style.order) - 1).toString()) {
        position = allBtns[i]
        position.classList.add('highlight')
        return
      }
    }
  }
}

// Start listening for keyboard events.
keyboardEvents()
