const version = '1.0.0'

self.addEventListener('install', event => {
  console.log('ServiceWorker: Installed version', version)

  /**
   * Cache assets.
   *
   * @returns {Promise} A promise.
   */
  const assets = async () => {
    const cache = await self.caches.open(version)
    console.log('Caching files...')
    return cache.addAll([
      'images/2202_R0lVIEFOTiAwNDAtODg.png',
      'images/2202_R0lVIEFOTiAwNDAtOTA.png',
      'images/2226_R0lVIEFOTiAwNDItMTAw.png',
      'images/2226_R0lVIEFOTiAwNDItMTAy.png',
      'images/2226_R0lVIEFOTiAwNDItMTE2.png',
      'images/2226_R0lVIEFOTiAwNDItNjQ.png',
      'images/2226_R0lVIEFOTiAwNDItNzQ.png',
      'images/2227_R0lVIEFOTiAwNDYtODI.png',
      'images/cardback.png',
      'images/cotton-candy.png',
      'images/letter.png',
      'images/panda.png',
      'images/panda-copy.png',
      'images/pen.png',
      'images/cursor.png',
      'css/styles.css'
    ])
  }
  event.waitUntil(assets())
})

/**
 * Fetch caches assets.
 *
 * @param {object} request A request.
 * @returns {Promise} A promise.
 */
const cacheFetch = async request => {
  try {
    const response = await fetch(request)
    const cache = await self.caches.open(version)
    cache.put(request, response.clone())
    return response
  } catch (err) {
    console.info('ServiceWorker: Serving result.')
    return self.caches.match(request)
  }
}

self.addEventListener('fetch', event => {
  console.log('ServiceWorker: Fetching')

  event.respondWith(cacheFetch(event.request))
})

/**
 * Remove caches assets.
 *
 * @returns {Promise} A promise.
 */
const removeAssets = async () => {
  const keys = await self.caches.keys()
  return Promise.all(
    keys.map(cache => {
      if (cache !== version) {
        console.info('Clearing cache...', cache)
        return self.caches.delete(cache)
      }
      return undefined
    })
  )
}

self.addEventListener('activate', event => {
  console.log('ServiceWorker: Activated version', version)
  event.waitUntil(removeAssets())
})
